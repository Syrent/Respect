package dev.syrent.respect;

import org.bukkit.plugin.java.JavaPlugin;

public final class Respect extends JavaPlugin {

    @Override
    public void onEnable() {
        // Plugin startup logic
        this.getServer().getConsoleSender().sendMessage("§b[Respect] §aPlugin enabled!");
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
        this.getServer().getConsoleSender().sendMessage("§b[Respect] §cPlugin disabled!");
    }
}
